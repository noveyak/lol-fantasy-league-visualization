var express = require('express');

var app = express();
var http = require('http').Server(app);

var path = require('path');
var routes = require('./routes/router.js');

app.use(express.static(path.join(__dirname, 'public')));

app.use('/api', routes);

http.listen(8181, function(){
	console.log('listening on *:8181');
});
