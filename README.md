# Synopsis

This is a toy project used to examine the accuracy of Riot LCS Prediction in the fantasy league they run every season. It calculates some general summary statistics such as standard deviation, means, etc but also finds the Pearson correlation coefficient to determine the linear correlation between predictions and actual performance. Charts are added to help users better identify trends in predictions.

The app was built to primarily to play with React and Flux(Reflux) libraries. Areas of interest was the general application model and integration between React and other libraries such as d3.js. The statics portion of the application ended up taking more effort than expected because of the lack of a strong stat library in JS that could do integrations, p-value lookups, and other math functions.

# Demo

Working demo can be found for NA/EU LCS Season 8 can be found [here](http://brian.li/lolstats).

# Setting it up

Running this locally can be done fairly easily after cloning the repository assuming you have node installed.

```bash
$ npm install
```

Build the client

```bash
$ npm run build
```

Start the server:

```bash
$ npm start
```

# License
[MIT License](http://opensource.org/licenses/MIT)
