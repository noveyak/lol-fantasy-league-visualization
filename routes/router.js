var _ = require('lodash');
var express = require('express');
var router = express.Router();

var data = require('../model/data8');

var positionToNameMap = {
    'AD_CARRY': 'ADC',
    'MID_LANE': 'Mid',
    'TOP_LANE': 'Top',
    'SUPPORT': 'Support',
    'JUNGLER': 'Jungler'
};

var scoreMap = {
    kills: 2,
    deaths: -0.5,
    assists: 1.5,
    minionKills: 0.01,
    tripleKills: 2,
    quadraKills: 5,
    pentaKills: 10,
    killOrAssistBonus: 2
};

function generateScores(stats){
    var score = {
        actual: 0,
        expected: 0
    };

    for(var key in scoreMap){
        score.actual += scoreMap[key] * stats[key].actualValue;
        score.expected += scoreMap[key] * stats[key].projectedValue;
    }

    return score;
}


var players = {};

for (var k in data.proPlayers){
    var player = data.proPlayers[k];
    player.position = positionToNameMap[player.positions[0]];
    player.team = data.proTeams[player.proTeamId].name;

    _.forEach(player.statsByWeek, function(weeklyStat){
        var weeklyScore = generateScores(weeklyStat);
        weeklyStat.score = {
            actualValue: weeklyScore.actual,
            projectedValue: weeklyScore.expected
        };
    });

    //Don't add players that haven't done anything all season
    if(player.statsBySeason.assists.actualValue !== 0){
        players[player.name] = player;
    }
}

router.get('/search-terms', function(req, res) {
	//console.log(req.param("term"))
    var searchTerms = _.map(players, function(player){
        return {type: 'playerName', name: player.name, team:player.team, position:player.position};
    });
    res.json(searchTerms);
});

router.get('/players', function(req, res) {
    var returnObj = {};

    _.forEach(req.query.names, function(name){
        returnObj[name] = players[name];
    });

    res.json(returnObj);
});


module.exports = router;
