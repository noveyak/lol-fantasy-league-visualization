var Reflux = require('reflux');
var _ = require('lodash');
var playerStore = require('./PlayerStore');
var actions = require('../actions/actions');
var stat = require('../utils/stat');
var Constants = require('../utils/Constants');

var StatStore = Reflux.createStore({
    init: function(){
        this.stats = {
            summary: {},
            distribution: {},
            fit: {}
        };
        this.listenTo(playerStore, this.onPlayersLoaded);
    },
    calculateSummaryStats: function(data){
        //console.log('DATA', data);

        var summaryStats = {
            highestScore: {
                //Comparator function is used to determine if need to be replaced
                comparator: function(max, test){
                    return max < test;
                },
                //Value stores the function to return the value for the test
                value: function(score){
                    return score.actualValue;
                }
            },
            lowestScore: {
                comparator: function(min, test){
                    return min > test;
                },
                value: function(score){
                    return score.actualValue;
                }
            },
            projectedHighestScore: {
                comparator: function(max, test){
                    return max < test;
                },
                value: function(score){
                    return score.projectedValue;
                }
            },
            projectedLowestScore: {
                comparator: function(min, test){
                    return min > test;
                },
                value: function(score){
                    return score.projectedValue;
                }
            },
            highestDifferential: {
                comparator: function(max, test){
                    return max < test;
                },
                value: function(score){
                    return Math.abs(score.actualValue - score.projectedValue);
                }
            },
            lowestDifferential: {
                comparator: function(min, test){
                    return min > test;
                },
                value: function(score){
                    return Math.abs(score.actualValue - score.projectedValue);
                }
            }
        };

        var statResults = _.reduce(_.map(data, function(playerData){
            return _.reduce(playerData.statsByWeek, function(result, stats, week){

                var test = _.mapValues(summaryStats, function(field, key){
                    return {
                        currentValue: field.value(stats.score),
                        actualValue: stats.score.actualValue,
                        projectedValue: stats.score.projectedValue,
                        week: week,
                        name: playerData.name
                    };
                });

                return _.mapValues(summaryStats, function(field, key){
                    return stats.score.actualValue !== 0 && (!result[key] || field.comparator(result[key].currentValue, test[key].currentValue)) ? test[key] : result[key];
                });

            }, {});
        }), function(result, test){
            return _.mapValues(summaryStats, function(field, key){
                return field.comparator(result[key].currentValue, test[key].currentValue) ? test[key] : result[key];
            });
        });

        //console.log(statResults);
        return statResults;
    },
    calculateDistributionStats: function(data){
        var scores = _.reduce(
            _.map(data, function(playerData){
                return _.reduce(playerData.statsByWeek, function(result, stats){
                    if(stats.score.actualValue !== 0){
                        result.actual.push(stats.score.actualValue);
                        result.projected.push(stats.score.projectedValue);
                    }
                    return result;
                }, {actual:[], projected:[]});
            }),
            function(result, playerStat){
                return {actual: result.actual.concat(playerStat.actual), projected: result.projected.concat(playerStat.projected)};
            }
        );

        var distributionStat =  {
            stdDeviation: {
                actualValue: stat.stdDeviation(scores.actual),
                projectedValue: stat.stdDeviation(scores.projected),
            },
            medianAbsoluteDeviation: {
                actualValue: stat.medianAbsoluteDeviation(scores.actual),
                projectedValue: stat.medianAbsoluteDeviation(scores.projected)
            }
        };

        return distributionStat;
    },
    calculateGoodnessOfFit: function(data){
        // console.log('GOT DATA', data);

        var dataPoints = _.reduce(_.map(data, function(playerData){
            return _.reduce(playerData.statsByWeek, function(result, stats){
                if(stats.score.actualValue !== 0){
                    result.push(stats.score);
                }
                return result;
            }, []);
        }), function(result, playerStat){
            return result.concat(playerStat);
        });

        var r = stat.pearsonR(dataPoints);
        var df = dataPoints.length - 2;

        return {
            r: r,
            p: stat.tStat_Pvalue(stat.r_tStat(r, df), df),
            df: df,
            scatterPoints: dataPoints
        };

        // console.log('DATAPOINTS', dataPoints.length);
        // console.log('RSQR', rSqr);
        //console.log('PVALUE', stat.chiSqrToPvalue(chiSqr, dataPoints.length - 1));
    },
    onPlayersLoaded: function(playerStore){

        //Wait to trigger until we have all the player data
        if(playerStore.state === Constants.LOADED){

            var playerData = _.filter(_.pluck(playerStore.storeData, 'data'));

            if(playerData.length){
                this.stats = {
                    summary: this.calculateSummaryStats(playerData),
                    distribution: this.calculateDistributionStats(playerData),
                    fit: this.calculateGoodnessOfFit(playerData)
                };

                this.trigger(this.stats);
            } else {
                this.trigger(null);
            }
        }

    },
});

module.exports = StatStore;
