var Reflux = require('reflux');
var _ = require('lodash');
var actions = require('../actions/actions');
var Constants = require('../utils/Constants');

var _allPlayers = {
    state: Constants.LOADED,
    storeData: {}
};

var colorCollection = ['#1f77b4', '#aec7e8', '#ff7f0e', '#ffbb78', '#2ca02c', '#98df8a', '#d62728', '#ff9896', '#9467bd', '#c5b0d5',
                       '#8c564b', '#c49c94', '#e377c2', '#f7b6d2', '#7f7f7f',' #c7c7c7', '#bcbd22', '#dbdb8d', '#17becf', '#9edae5'];

var colorSet = colorCollection.slice(0);

var PlayerStore = Reflux.createStore({
    listenables: actions,
    init: function(){
        //this.listenTo(actions.loadSeason.completed, this.onLoadSeason);
    },
    _getAvailableColor: function(){
        var color = _.sample(colorSet);
        colorSet = _.pull(colorSet, color);
        //Need to reinitialize the colorSet if the last color is taken
        if(colorSet.length === 0)
            colorSet = colorCollection.slice(0);

        return color;
    },
    onAddPlayers: function(names){

        //Setting to null to signify data is loading
        _.forEach(names, function(playerName){
            _allPlayers.storeData[playerName] = {
                color: this._getAvailableColor(),
                data: null
            };
        }.bind(this));

        _allPlayers.state = Constants.LOADING;

        this.trigger(_allPlayers);
    },
    onSetPlayers: function(names){

        //Reset players
        _allPlayers.storeData = {};
        //Reset colors
        colorSet = colorCollection.slice(0);

        this.onAddPlayers(names);

    },
    onAddPlayersCompleted: function(data){
        for(var playerName in data){
            _allPlayers.storeData[playerName].data = data[playerName];
        }
        _allPlayers.state = Constants.LOADED;
        this.trigger(_allPlayers);
    },
    onSetPlayersCompleted: function(data){

        //No need to do anything different from add here becuase the setter already cleared the existing players
        console.log(data);

        this.onAddPlayersCompleted(data);
    },
    onRemovePlayers: function(playerNames){

        var namesToRemove = _.filter(playerNames, function(playerName){
            return _allPlayers.storeData[playerName];
        });

        if(namesToRemove.length){
            _.forEach(namesToRemove, function(playerName){
                //Add colors back into pool to be used if its not already in there
                if (colorSet.indexOf(_allPlayers.storeData[playerName].color) == -1)
                    colorSet.push(_allPlayers.storeData[playerName].color);

                delete _allPlayers.storeData[playerName];
            });

            this.trigger(_allPlayers);

        }

    },
    getPlayers: function(){
        return _allPlayers.storeData;
    }
});

module.exports = PlayerStore;
