var Reflux = require('reflux');
var actions = require('../actions/actions');

var SearchStore = Reflux.createStore({
    listenables: actions,
    init: function(){
        this.searchTerms = [];
    },
    onLoadSeasonCompleted: function(data){
        //console.log(data);
        this.searchTerms = data;
        this.trigger(this.searchTerms);
    },
});

module.exports = SearchStore;
