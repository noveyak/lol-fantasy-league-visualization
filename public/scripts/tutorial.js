// tutorial1.js
var data = [
  {author: "Pete Hunt", text: "This is one comment"},
  {author: "Jordan Walke", text: "This is *another* comment"},
  {author: "Brian Li", text: "This is Brian's test comment"}
];

var CommentBox = React.createClass({
    handleCommentSubmit: function(comment){
        var comments = this.state.data;
        var newComments = comments.concat([comment]);

        console.log(newComments);

        this.setState({
            data: newComments.slice(-3)
        });

        console.log(this.state.data);

    },
    getInitialState: function() {
        return {data: data};
    },
    render: function() {
        return (
            <div className="commentBox">
                <h1>Comments</h1>
                <CommentList data={this.state.data} />
                <CommentForm onCommentSubmit={this.handleCommentSubmit} />
            </div>
        );
    }
});

var CommentList = React.createClass({
    render: function() {
        commentNodes = [];
        for(var i=0; i<this.props.data.length; i++){
            commentNodes.push(
                <Comment author={this.props.data[i].author}>
                    {this.props.data[i].text}
                </Comment>
            );
        }

        return (
            <div className="commentList">
                {commentNodes}
            </div>
        );
    }
});

var CommentForm = React.createClass({
    addComment: function(evt){
        evt.preventDefault();
        this.props.onCommentSubmit({
            author: React.findDOMNode(this.refs.name).value.trim(),
            text: React.findDOMNode(this.refs.text).value.trim()
        });
    },
    render: function() {
        return (
            <form className="commentForm">
                <input type="text" ref="name" placeholder="Your name" />
                <input type="text" ref = "text" placeholder="Say something..." />
                <button type="submit" ref="submit" onClick={this.addComment}>
                    Add Comment
                </button>
            </form>
        );
    }
});

var Comment = React.createClass({
  render: function() {
    return (
      <div className="comment">
        <h2 className="commentAuthor">
          {this.props.author}
        </h2>
        {this.props.children}
      </div>
    );
  }
});

React.render(
    <CommentBox data={data} />,
    document.getElementById('content')
);
