var React = require('react/addons');
var Reflux = require('reflux');
var _ = require('lodash');

var actions = require('./actions/actions');

var playerStore = require('./stores/PlayerStore');
var searchStore = require('./stores/SearchStore');
var statStore = require('./stores/StatStore');

var Searchbar = require('./components/Searchbar.jsx');
var Statbar = require('./components/Statbar.jsx');
var PerformanceChart = require('./components/PerformanceChart.jsx');

var Constants = require('./utils/Constants');


var containerNode = document.getElementById('content');

var App = React.createClass({
    getInitialState: function() {
        return {
            playerData: {},
            loadedPlayerData: {},
            performanceChartDimensions: {},
            scatterPlotDimensions: {},
            stats: null,
            season: 8,
            appState: Constants.LOADED
        };
    },
    mixins: [React.addons.LinkedStateMixin, Reflux.ListenerMixin],
    updateDimensions: function() {

        //This is the width including scrollbars which is useful for seeing what responsive css will be applied
        var windowWidth = window.innerWidth;

        var scatterPlotSettings = [
            {windowWidth: 1440, chartWidth: 410, chartHeight:410},
            {windowWidth: 1200, chartWidth: 376, chartHeight: 376},
            {windowWidth: 0, chartWidth: 320, chartHeight: 360}
        ];

        var scatterPlotDimensions;

        for(var i=0; i<scatterPlotSettings.length; i++){
            var setting = scatterPlotSettings[i];
            if(windowWidth > setting.windowWidth){
                scatterPlotDimensions = {
                    width: _.max([0.25 * $(containerNode).width(), setting.chartWidth]),
                    height: setting.chartHeight
                };
                break;
            }
        }

        //Need to do some js calculations to keep svgs scaled properly - init to something that will just blank out height
        var topSectionHeight = $(React.findDOMNode(this.refs.topSectionNode)).height() || $(window).height() - topSectionHeight - 5;

        this.setState({
            performanceChartDimensions: {
                width:  $(containerNode).width(),
                height: _.max([$(window).height() - topSectionHeight - 5, 450])
            },
            scatterPlotDimensions: scatterPlotDimensions
        });
    },
    componentDidMount: function() {
        this.updateDimensions();
        window.addEventListener("resize", this.updateDimensions);
    },
    componentWillMount: function() {
        actions.loadSeason(this.state.season);
        this.listenTo(playerStore, this.onPlayersChange);
        this.listenTo(statStore, this.handleStatsUpdate);
        this.updateDimensions();
    },
    handleStatsUpdate: function(data){
        this.setState({
            stats: data
        });
    },
    onPlayersChange: function(playerStore){
        if(playerStore.state == Constants.LOADED){
            this.setState({
                playerData: playerStore.storeData,
                loadedPlayerData: playerStore.storeData,
                appState: playerStore.state
            });
        } else {
            this.setState({
                playerData: playerStore.storeData,
                appState: playerStore.state
            });
        }

    },
    render: function() {
        return (
            <div className="app">
                <div className="topSectionContainer" ref="topSectionNode">
                    <header>
                        <h1>Fantasy League Prediction Modeling For LCS</h1>
                        <span>Evaluating Riot projections vs actual performance</span>
                    </header>

                    <Searchbar data={this.state.playerData} appState={this.state.appState}/>

                    <Statbar data={this.state.stats} scatterPlotDimensions={this.state.scatterPlotDimensions}/>
                </div>

                <PerformanceChart data={this.state.loadedPlayerData} dimensions={this.state.performanceChartDimensions} />

            </div>
        );
    }
});

React.render(
    <App />,
    containerNode
);
