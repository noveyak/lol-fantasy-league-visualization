var Reflux = require('reflux');

var actions = Reflux.createActions({
    loadSeason: {asyncResult: true},
    addPlayers: {asyncResult: true},
    setPlayers: {asyncResult: true},
    removePlayers: {}
});

actions.loadSeason.listen(function(season){
    $.get('/api/search-terms').then(function(data){
        this.completed(data);
    }.bind(this));
});

actions.addPlayers.listen(function(names){
    $.get('/api/players', {
        names: names
    }).then(function(data){
        this.completed(data);
    }.bind(this));
});

actions.setPlayers.listen(function(names){
    $.get('/api/players', {
        names: names
    }).then(function(data){
        this.completed(data);
    }.bind(this));
});


module.exports = actions;
