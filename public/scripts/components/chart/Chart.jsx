var React = require('react/addons');
var _ = require('lodash');

var Chart = React.createClass({
    componentWillMount: function(){
        this.createScales(this.props);
    },
    componentWillReceiveProps: function(nextProps){
        this.createScales(nextProps);
    },
    createScales: function(props){
        this.setState({
            yScale: d3.scale.linear()
                .domain(props.yDomain)
                .range([props.height - 40, 0]),

            xScale: d3.scale.linear()
                .domain(props.xDomain)
                .range([0, props.width - 50])
        });
    },
    render: function() {

        var chartWidth = this.props.width - 50;
        var chartHeight = this.props.height - 40;
        var chartStyles = {margin: '0 10px 40px 40px'};

        var dataSeries = _.map(this.props.dataSeries, function(d){
            var dataSeriesFactory = React.createFactory(d);
            return dataSeriesFactory({data: this.props.data, xScale: this.state.xScale, yScale: this.state.yScale, key: d});
        }.bind(this));

        return (
            <svg className="chart" width={chartWidth} height={chartHeight} style={chartStyles}>

                {dataSeries}

                <Axis data={this.props.data}
                    xScale={this.state.xScale} yScale={this.state.yScale}
                    xAxisText={this.props.xAxisText} yAxisText={this.props.yAxisText}
                    chartWidth={chartWidth}
                    chartHeight={chartHeight} />

            </svg>
        );
    }
});


var Axis = React.createClass({
    componentDidMount: function(){
        this.buildAxis();
    },
    componentDidUpdate: function(){
        this.buildAxis();
    },
    buildAxis: function(){
        var xAxis = d3.svg.axis()
            .scale(this.props.xScale)
            .orient("bottom");

        var yAxis = d3.svg.axis()
            .scale(this.props.yScale)
            .orient("left");

        d3.select(this.refs.xAxisContainer.getDOMNode())
            .call(xAxis);

        d3.select(this.refs.yAxisContainer.getDOMNode())
            .call(yAxis);

    },
    render: function() {
        var xAxisTransformation = 'translate(0,' + this.props.chartHeight + ')';

        return (
            <g className="axisContainer">
                <g ref="xAxisContainer" transform={xAxisTransformation} >
                    <text textAnchor='end' y="-10" width="410" transform={'translate(' + this.props.chartWidth + ', 0)'}>{this.props.xAxisText}</text>
                </g>
                <g ref="yAxisContainer">
                    <text textAnchor='end' y="20" width="410" transform={'translate(' + 0  + ', 0) rotate(-90)'}>{this.props.yAxisText}</text>
                </g>
            </g>
        );
    }
});

module.exports = Chart;
