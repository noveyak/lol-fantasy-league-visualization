var React = require('react/addons');
var Reflux = require('reflux');
var _ = require('lodash');
var stat = require('../utils/stat');
var statStore = require('../stores/StatStore');
var Chart = require('./chart/Chart.jsx');

function formatScore(score){
    return parseFloat(score.toFixed(2));
}

function formatStat(score){
    return parseFloat(score.toFixed(3));
}

var StatChart = React.createClass({
    render: function() {

        var values = this.props && this.props.points ? _.pluck(this.props.points, 'projectedValue').concat(_.pluck(this.props.points, 'actualValue')) : [];
        // console.log('VALUES', values);

        var minValue = Math.floor(_.min(values)/10) * 10;
        var maxValue = Math.ceil(_.max(values)/10) * 10;

        //Passing data series in as a prop instead of child jsx becuase I need to access state being created by Chart.
        return (
            <Chart width={this.props.dimensions.width} height={this.props.dimensions.height}
                data={this.props.points}
                xDomain={[minValue, maxValue]}
                yDomain={[minValue, maxValue]}
                dataSeries= {[DataSeries]}
                xAxisText={"Projected"}
                yAxisText={"Actual"}
                 >
            </Chart>
        );
    }
});

var DataSeries = React.createClass({
    getInitialState: function() {
        return {
            assignedColors: {}
        };
    },
    render: function() {
        var props = this.props;

        var circles = _.map(this.props.data, function(point){
            // console.log(point)
            var cx = this.props.xScale(point.projectedValue);
            var cy = this.props.yScale(point.actualValue);

            return (<circle key={cx + '_' + cy} cx={cx} cy={cy} r="3" />);

        }.bind(this));

        return (
            <g>
                {circles}
            </g>
        );
    }
});

var StatTable = React.createClass({
    render: function(){
        var fieldRows = [
            [
                {type: 'summary', field: 'highestScore', title:'Highest Score'},
                {type: 'summary', field: 'lowestScore', title: 'Lowest Score'},
                {type: 'summary', field: 'highestDifferential', title: 'Largest Differential'},
                {type: 'distribution', field: 'stdDeviation', title: 'Standard Deviation'}
            ],
            [
                {type: 'summary', field: 'projectedHighestScore', title:'Projected Highest Score'},
                {type: 'summary', field: 'projectedLowestScore', title: 'Projected Lowest Score'},
                {type: 'summary', field: 'lowestDifferential', title: 'Smallest Differential'},
                {type: 'distribution', field: 'medianAbsoluteDeviation', title: 'Median Absolute Deviation'}
            ]
        ];

        var stats = this.props.data;

        //console.log('STATS IS', stats);

        var tableRows = _.map(fieldRows, function(row, idx){

            var titleCells = _.map(row, function(field){
                return (<td key={'_title.' + field.field} className="statEntryTitle">{field.title}</td>);
            });

            var subContentCells = _.map(row, function(field){
                var subContent;

                if(stats && stats[field.type][field.field].name){
                    subContent = <div><div className="playerValue">{stats[field.type][field.field].name}</div><div className="weekValue">Week {stats[field.type][field.field].week}</div></div>;
                } else {
                    subContent = <div>&nbsp;</div>;
                }

                return(<td key={'_subcontent.' + field.field} className="statEntrySubcontent">{subContent}</td>);
            });

            var scoreCells = _.map(row, function(field){

                var actualValue = stats ? formatScore(stats[field.type][field.field].actualValue) : '-';
                var projectedValue = stats ? formatScore(stats[field.type][field.field].projectedValue) : '-';

                return(<td key={'_content.' + field.field} className="statEntryContent">
                    <div className="actualValue">
                        <div className="score">{actualValue}</div>
                        <div className="scoreTitle">Actual</div>
                    </div>
                    <div className="projValue">
                        <div className="score">{projectedValue}</div>
                        <div className="scoreTitle">Projected</div>
                    </div>
                </td>);
            });

            return [
                <tr key={'_titleRow.' + idx}>{titleCells}</tr>,
                <tr key={'_subcontentRow.' + idx}>{subContentCells}</tr>,
                <tr key={'_scoreRow.' + idx}>{scoreCells}</tr>
            ];
        });

        var r = stats ? formatStat(stats.fit.r) : '-';
        var df = stats ? formatStat(stats.fit.df) : '-';
        var p = stats ? formatStat(stats.fit.p) : '-';

        var fitRow = (<tr className="fitRow"><td colSpan={fieldRows[0].length}>
            <span className="statEntryTitle">Summary Statistics:</span>
            <span className="scoreTitle">r =</span><span className="score">{r}</span>
            <span className="scoreTitle">df =</span><span className="score">{df}</span>
            <span className="scoreTitle">p &nbsp;{p < 0.001 ? '<' :  '='}</span><span className="score">{p == '-' || p > 0.001 ? p : '.001'}</span>
        </td></tr>);

        return (
            <table className='statTable'>
                <tbody>
                    {tableRows}
                    {fitRow}
                </tbody>
            </table>
        );
    }
});

var Statbar = React.createClass({
    render: function(){

        var scatterPoints = this.props.data ? this.props.data.fit.scatterPoints : [];

        return (
            <div className="statBar">
                <div className="tableWrapper">
                    <StatTable data={this.props.data} />
                </div>
                <div className="scatterPlotWrapper">
                    <StatChart points={scatterPoints} dimensions={this.props.scatterPlotDimensions}/>
                </div>
            </div>);
    }
});

module.exports = Statbar;
