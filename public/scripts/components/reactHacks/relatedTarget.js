var React = require('react');
require("setimmediate");

function relatedTarget(e, fn) {
    // jshint browser: true
    var target = e.relatedTarget;

    setImmediate(function() {
        var to = target || document.activeElement;
        fn(to);
    });
}

relatedTarget.contains = function(root, to) {

    root = React.findDOMNode(root);
    to = React.findDOMNode(to);

    return root === to || $.contains(root, to);
};

module.exports = relatedTarget;
