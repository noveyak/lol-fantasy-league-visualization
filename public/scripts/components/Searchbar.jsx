var React = require('react/addons');
var Reflux = require('reflux');
var classNames = require('classnames');
var TimeoutTransitionGroup = require('../react-components/js/timeout-transition-group.jsx');

var _ = require('lodash');

var playerStore = require('../stores/PlayerStore');
var searchStore = require('../stores/SearchStore');

var actions = require('../actions/actions');

var relatedTarget = require('./reactHacks/relatedTarget');

var Constants = require('../utils/Constants');

var Searchbar = React.createClass({
    mixins: [Reflux.ListenerMixin],
    getInitialState: function() {
        return {
            inputText:'',
            showingOptions: false,
            focusOptionIdx: -1,
        };
    },
    componentWillMount: function() {
        this.listenTo(searchStore, this.handleSearchTermsUpdate);
        //
        // this.debouncedSetSearchText = _.debounce(function(evt){
        //     this.setSearchText(evt.target.value);
        // }, 200);

    },
    inputKeydownMap: {
      40: 'focusNextOption',
      27: 'handleEscButton',
      8: 'possiblyRemoveLastPlayer'
    },
    optionKeydownMap: {
      38: 'focusPrevOption',
      40: 'focusNextOption',
      13: 'selectOption',
      27: 'handleEscButton'
    },
    handleSearchTermsUpdate: function(data){
        this.setState({searchTerms: data});
    },
    handleEscButton: function(){
        this.focusInput();
        this.setState({
            focusOptionIdx: -1,
            showingOptions: false
        });
    },
    handleInputTextChange: function(evt){
        evt.persist();
        //Making sure we don't super overload text change
        var inputText = evt.target.value;

        this.setState({
            showingOptions: true,
            inputText: inputText
        });
    },
    handleInputBlur: function(evt){
        //Do not blur if moving focus to an option
        relatedTarget(evt, function(target){
            if(!target || target.getAttribute('role') !== 'option')
                this.hideOptions();
        }.bind(this));

    },
    handleInputClick: function(){
        this.focusInput();
    },
    handleInputKeyDown: function(evt){
        if(this.inputKeydownMap.hasOwnProperty(evt.keyCode)){
            this[this.inputKeydownMap[evt.keyCode]](evt);
        }
    },
    handleOptionMouseEnter: function(evt, option){
        this.setState(function(previousState, currentProps) {
            return {focusOptionIdx: option.idx};
        }, this.focusCurrentOption);
    },
    handleOptionClick: function(evt, option){
        this.selectOption(evt, option);
        evt.preventDefault();
    },
    handleOptionKeyDown: function(evt, option){
        if(this.optionKeydownMap.hasOwnProperty(evt.keyCode)){
            this[this.optionKeydownMap[evt.keyCode]](evt, option);
        }
        evt.preventDefault();
    },
    handleTokenClick: function(evt, token){
        this.removePlayers([token.name]);
    },
    handleRandomClick:function(evt){
        this.setPlayers(_.pluck(_.sample(this.state.searchTerms, 5), 'name'));
    },
    handleClearAllClick:function(evt){
        this.removePlayers(Object.keys(playerStore.getPlayers()));
        evt.preventDefault();
    },
    addPlayers: function(names){
        actions.addPlayers(names);
    },
    removePlayers: function(names){
        actions.removePlayers(names);
    },
    setPlayers: function(names){
        actions.setPlayers(names);
    },
    possiblyRemoveLastPlayer: function(){
        //Only remove players if backspacking over empty and there are existing players
        var playerKeys = Object.keys(this.props.data);
        if(React.findDOMNode(this.refs.searchbarInput).value.length === 0 && playerKeys.length > 0){
            this.removePlayers([playerKeys[playerKeys.length - 1]]);
        }
    },
    hideOptions: function(option){
        this.setState({
            showingOptions: false
        });
    },
    showOptions: function(option){
        this.setState({
            showingOptions: true
        });
    },
    focusInput: function(option){
        React.findDOMNode(this.refs.searchbarInput).focus();
        this.setState({
            showingOptions: true,
            focusOptionIdx: -1
        });
    },
    focusNextOption: function(evt, option){

        var optionsLength = this.getFilteredOptions().length;

        if(optionsLength){
            this.setState(function(previousState, currentProps) {
                return {
                    showingOptions: true,
                    focusOptionIdx: this.state.showingOptions ? (previousState.focusOptionIdx + 1) % optionsLength : 0
                };
            }, this.focusCurrentOption);
        }

        evt.preventDefault();
    },
    focusPrevOption: function(evt, option){
        this.setState(function(previousState, currentProps) {

            var optionsLength = this.getFilteredOptions().length || 1;

            var prevOptionid = previousState.focusOptionIdx - 1;

            return {focusOptionIdx: (prevOptionid < 0) ? prevOptionid + optionsLength : prevOptionid };

        }, this.focusCurrentOption);
        evt.preventDefault();
    },
    focusCurrentOption: function(){
        //Only focus nodes if it exists
        if(this.state.focusOptionIdx >= 0 && React.findDOMNode(this.refs.autocompleteResults).childNodes.length > this.state.focusOptionIdx)
            React.findDOMNode(this.refs.autocompleteResults).childNodes[this.state.focusOptionIdx].focus();
    },
    selectOption: function(evt, option){
        this.addPlayers([option.data.name]);
        //Need to clear placeholder just temporarily to prevent flashing when state is set but player is not yet added
        React.findDOMNode(this.refs.searchbarInput).placeholder = '';
        this.setState({
            inputText: ''
        });
        this.focusInput();
    },
    getFilteredOptions: function(){
        var searchText =  this.state.inputText.trim().toUpperCase();

        if(searchText.length > 0){
            return _.filter(this.state.searchTerms, function(searchOption){
                return searchOption.name.toUpperCase().indexOf(searchText) > -1 && !this.props.data.hasOwnProperty(searchOption.name);
            }.bind(this)).slice(0, 10);
        } else {
            return [];
        }
    },
    render: function(){

        var autocompleteResults = null;

        if(this.state.showingOptions){
            autocompleteResults = _.map(this.getFilteredOptions(), function(searchOption, idx){
                return (
                    <SearchOption
                        key={searchOption.name}
                        data={searchOption}
                        inputText={this.state.inputText}
                        idx={idx}
                        focused={this.state.focusOptionIdx == idx}
                        handleClick={this.handleOptionClick}
                        handleKeyDown={this.handleOptionKeyDown}
                        handleMouseEnter={this.handleOptionMouseEnter}
                    />
                );
            }.bind(this));
        }

        var searchbarTokens = _.map(this.props.data, function(player, playerName){
            return (
                <SearchToken key={playerName} name={playerName} color={player.color} handleClick={this.handleTokenClick} />
            );
        }.bind(this));

        var loadingIcon = (
            <TimeoutTransitionGroup transitionName="fadingLoad" enterTimeout={100} leaveTimeout={300}>
                {this.props.appState === Constants.LOADING ? <div className='loadingIcon loading'></div> : null}
            </TimeoutTransitionGroup>
        );

        return (
            <div className="searchWrapper">
                <div className="searchbar">
                    <div className="searchbarInputWrapper" onBlur={this.handleInputBlur} onClick={this.handleInputClick} >
                        <ul className="searchInputTokens">
                            {searchbarTokens}
                            <li className="searchbarTokenWrapper">
                                <input className="searchbarInput" type="text"
                                    ref="searchbarInput"
                                    onChange={this.handleInputTextChange}
                                    onKeyDown={this.handleInputKeyDown}
                                    onFocus={this.handleInputFocus}
                                    value={this.state.inputText}
                                    placeholder={!Object.keys(this.props.data).length ? 'Search for players...' : ''}
                                />
                            </li>
                        </ul>

                        {loadingIcon}

                    </div>
                    <ul className={classNames('results', {'noResults': !this.state.showingOptions || autocompleteResults.length === 0})} ref="autocompleteResults" onBlur={this.handleInputBlur}>
                        {autocompleteResults}
                    </ul>
                </div>
                <div className="searchButtons">
                    <div className="clearAllButton button" onClick={this.handleClearAllClick}>
                        <i className="fa fa-times fa-2x"></i>
                    </div>
                    <div className="searchIconButton button" onClick={this.handleRandomClick}>
                        <i className="fa fa-random fa-2x"></i>
                    </div>
                </div>
            </div>
        );
    }
});

var SearchOption = React.createClass({
    handleMouseDown: function(evt){
        //Keeping evt from blurring the input field
        evt.preventDefault();
    },
    handleMouseEnter: function(evt){
        this.props.handleMouseEnter(evt, this.props);
    },
    handleClick: function(evt){
        this.props.handleClick(evt, this.props);
    },
    handleKeyDown: function(evt){
        this.props.handleKeyDown(evt, this.props);
    },
    render: function(){

        var name = this.props.data.name;
        var inputText = this.props.inputText;

        var idx = name.toUpperCase().indexOf(inputText.trim().toUpperCase());

        return(
            <div tabIndex="-1"
                role="option"
                className={classNames('searchOption', {'focused': this.props.focused})}
                onMouseDown={this.handleMouseDown}
                onMouseEnter={this.handleMouseEnter}
                onClick={this.handleClick}
                onKeyDown={this.handleKeyDown}>

                <span className="searchOptionTitle">
                    {name.substring(0, idx)}
                    <strong>{name.substring(idx, idx + inputText.length)}</strong>
                    {name.substring(idx + inputText.length)}
                </span>

                <span className="searchOptionDescription">
                    {this.props.data.team}</span> - <span>{this.props.data.position}
                </span>

            </div>
        );
    }
});

var SearchToken = React.createClass({
    handleTokenClick: function(evt){
        evt.preventDefault();
        evt.stopPropagation();
    },
    handleCloseClick: function(evt){
        this.props.handleClick(evt, this.props);
    },
    render: function(){

        var style = {
            background: this.props.color
        };

        return(
            <li className="searchbarTokenWrapper">
                <div className="searchToken" onClick={this.handleTokenClick} style={style}>
                    <span className="searchTokenLabel">{this.props.name}</span>
                    <span className="searchTokenButton" onClick={this.handleCloseClick}><i className="fa fa-times"></i></span>
                </div>
            </li>
        );
    }
});

module.exports = Searchbar;
