var React = require('react/addons');
var TimeoutTransitionGroup = require('../react-components/js/timeout-transition-group.jsx');
var Reflux = require('reflux');
var _ = require('lodash');

var playerStore = require('../stores/PlayerStore');

var actions = require('../actions/actions');

var Chart = require('./chart/Chart.jsx');
var LayeredComponentMixin = require('./mixins/LayeredComponentMixin');

function formatScore(score){
    return parseFloat(score.toFixed(1));
}

var PerformanceChart = React.createClass({

    render: function() {

        return (
            <Chart width={this.props.dimensions.width} height={this.props.dimensions.height}
                data = {this.props.data}
                xDomain = {[1, 9]}
                yDomain = {[0, 100]}
                dataSeries = {[DataSeries]}
                xAxisText={"Week"}
                yAxisText={"Points"}
            />
        );
    }
});

var Circle = React.createClass({
    mixins: [LayeredComponentMixin],
    getInitialState: function() {
        return {
            showingTooltip: false
        };
    },
    renderLayer: function(evt){
        if(this.state.showingTooltip){
            return (
                <TimeoutTransitionGroup transitionName="fadingTooltip" enterTimeout={100} leaveTimeout={100}>
                    <WeeklyStatsTooltip key={this.props.data} data={this.props.data} week={this.props.week} attachNode={this.getDOMNode()}/>
                </TimeoutTransitionGroup>
            );
        } else {
            return <TimeoutTransitionGroup transitionName="fadingTooltip" enterTimeout={100} leaveTimeout={100} />;
        }
    },
    showTooltip: function() {
        this.setState({showingTooltip: true});
    },
    hideTooltip: function() {
        this.setState({showingTooltip: false});
    },
    render: function() {
        return (
            <circle fill={this.props.color} stroke={this.props.color} onMouseEnter={this.showTooltip} onMouseLeave={this.hideTooltip}
                cy={this.props.cy} cx={this.props.cx}
                r="6" className={this.props.type}
            />
        );
    }
});

var WeeklyStatsTooltip = React.createClass({
    componentDidMount : function(){
        //React.findDOMNode(this).focus();
        $node = $(React.findDOMNode(this));

        var componentTop =  this.props.attachNode.getBoundingClientRect().top + window.pageYOffset;
        var componentLeft = this.props.attachNode.getBoundingClientRect().left  + window.pageXOffset;

        var componentHeight = React.findDOMNode(this).clientHeight;
        var componentWidth = React.findDOMNode(this).clientWidth;

        // console.log('WIDTHS AND LEFTS', componentWidth, componentLeft);
        // console.log('COMPONENT HEIGHT IS', componentHeight);

        var divStyles = {};

        if(componentWidth + componentLeft + 30 >= $(window).width()){
            divStyles.left = componentLeft - componentWidth - 21;
            $node.addClass('rightConnector');
        } else {
            divStyles.left = componentLeft + 30;
            $node.addClass('leftConnector');
        }

        if(componentTop + componentHeight >= $(window).height()){
            divStyles.top = componentTop- 0.8 *  componentHeight + 5;
            $node.addClass('bottomConnector');
        } else {
            divStyles.top = componentTop - 0.2 *  componentHeight + 5;
            $node.addClass('topConnector');
        }

        $node.css(divStyles);

    },
    render: function(){

        console.log(this.props.data);

        var weekStats = this.props.data.statsByWeek[this.props.week];

        var columns = [
            [
                {label: 'Kills', value:'kills'},
                {label: 'Assists', value:'assists'},
                {label: 'Deaths', value:'deaths'},
                {label: 'Creeps', value:'minionKills'},
            ],
            [
                {label: 'Bonus', value:'killOrAssistBonus'},
                {label: '3x', value:'tripleKills'},
                {label: '4x', value:'quadraKills'},
                {label: '5x', value:'pentaKills'}
            ]
        ];

        var fields = [];

        headingsColumns = [];

        _.forEach(columns, function(column, idx){
            headingsColumns.push(<td key={'_col_' + idx + '_blank'}></td>);
            headingsColumns.push(<td key={'_col_' + idx + '_actual'}>Actual</td>);
            headingsColumns.push(<td key={'_col_' + idx + '_projected'}>Proj</td>);
        });

        for(var i=0; i<columns[0].length; i++){
            var cells = [];
            for(var j=0; j<columns.length; j++){
                var category = columns[j][i];
                cells.push(<td key={category.label}>{category.label}</td>);
                cells.push(<td key={category.label + '_actualValue'}>{formatScore(weekStats[category.value].actualValue)}</td>);
                cells.push(<td key={category.label + '_projectedValue'}>{formatScore(weekStats[category.value].projectedValue)}</td>);
            }

            fields.push(<tr key={'_row_' + i}>{cells}</tr>);

        }

        return (
            <div className="tooltip" onBlur={this.props.onBlur}>
                <div className="tooltipTitleBar">
                    <h2>{this.props.data.name}</h2>
                    <span className="tooltipTitleAside">Week: {this.props.week}</span>
                </div>
                <div className="tootipSecondaryInfo">
                    {this.props.data.team} - {this.props.data.position}
                </div>
                <table className="tooltipStatsTable">
                    <tbody>
                        <tr>
                            {headingsColumns}
                        </tr>
                        {fields}
                        <tr className="scoreRow">
                            <td colSpan={columns.length * 3}>
                                <div className="scoreRowContents">
                                    <span className="categoryTitle">Total Score:</span>
                                    <span className="categoryValues">
                                        <span className="scoreTitle">Actual: </span>
                                        <span className="score">{formatScore(weekStats.score.actualValue)}</span>

                                        <span className="scoreTitle">Projected: </span>
                                        <span className="score">{formatScore(weekStats.score.projectedValue)}</span>
                                    </span>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        );
    }
});


var Line = React.createClass({
    render: function() {
        return (
            <path stroke={this.props.color} d={this.props.points} className={this.props.type}/>
        );
    }
});

var DataSeries = React.createClass({
    getInitialState: function() {
        return {
            assignedColors: {}
        };
    },
    render: function() {
        var props = this.props;

        var colors = d3.scale.category20();

        var circles = [];
        var lines = [];


        for(var playerName in this.props.data){

            var player = this.props.data[playerName].data;

            if(player === null)
                continue;


            var playerColor = this.props.data[playerName].color;

            var priceline = d3.svg.line()
                .x(function(d) { return d.x; })
                .y(function(d) { return d.y; });

            var currentActualLinePoints = [];
            var currentExpectedLinePoints = [];

            for(var week in player.statsByWeek){

                var score = player.statsByWeek[week].score;
                var xPoint = this.props.xScale(week);
                var actual = this.props.yScale(score.actualValue);
                var expected = this.props.yScale(score.projectedValue);

                //Dont draw weeks players did not play in
                if(score.actualValue !== 0){

                    circles.push(
                        <Circle key={'_actualPoint.' + playerName + '_' + week} type="actual" cy={actual} cx={xPoint} color={playerColor} data={player} week={week}/>
                    );


                    circles.push(
                        <Circle key={'_projectedPoint.' + playerName + '_' + week} type="expected" cy={expected} cx={xPoint} color={playerColor} data={player} week={week}/>
                    );

                    currentActualLinePoints.push({x:xPoint, y:actual});
                    currentExpectedLinePoints.push({x:xPoint, y:expected});

                } else {
                //It is 0 so draw the current line and create a new one

                    lines.push(
                        <Line key={'_actualLine.' + playerName + '_' + lines.length} type="actual" color={playerColor} points={priceline(currentActualLinePoints)} />
                    );

                    lines.push(
                        <Line key={'_projectedLine.' + playerName + '_' + lines.length}  type="expected" color={playerColor} points={priceline(currentExpectedLinePoints)} />
                    );

                    currentActualLinePoints = [];
                    currentExpectedLinePoints = [];

                }

            }

            lines.push(
                <Line key={'_actualLine.' + playerName + '_' + lines.length} type="actual" color={playerColor} points={priceline(currentActualLinePoints)} />
            );

            lines.push(
                <Line key={'_projectedLine.' + playerName + '_' + lines.length} type="expected" color={playerColor} points={priceline(currentExpectedLinePoints)} />
            );

            currentLinePoints = [];
            currentExpectedLinePoints = [];

        }

        return (
            <g>
                {lines}
                {circles}
            </g>
        );
    }
});

module.exports = PerformanceChart;
