var _ = require('lodash');

var statUtils = {
    incompleteGamma: function(s, x){

        var iterations = 1000;

        if(x < 0){
            return 0;
        }

        sx = (1/s) * Math.pow(x, s) * Math.exp(-x);

        var sum = 1;
        var multiplier = 1.0;

        for(var i=0; i < iterations; i++){
            multiplier *= (x/++s);
            sum += multiplier;
        }

        return sum * sx;
    },
    gamma: function(n) {
        var g = 7;
        var p = [0.99999999999980993, 676.5203681218851, -1259.1392167224028, 771.32342877765313, -176.61502916214059, 12.507343278686905, -0.13857109526572012, 9.9843695780195716e-6, 1.5056327351493116e-7];

        if(n < 0.5) {
            return Math.PI / Math.sin(n * Math.PI) / this.gamma(1 - n);
        } else {
            n--;
            var x = p[0];
            for(var i = 1; i < g + 2; i++) {
                x += p[i] / (n + i);
            }
            var t = n + g + 0.5;
            return Math.sqrt(2 * Math.PI) * Math.pow(t, (n + 0.5)) * Math.exp(-t) * x;
        }
    },
    tStat_Pvalue: function(t, df){
        var iterations = 1000;
        var start = 0, end = t;
        var stepSize = (end-start)/iterations;

        var sum = 0;

        for(var i=0; i<iterations + 1; i++){
            var c = stepSize * i;
            var e = Math.pow(1 + Math.pow(c, 2)/df, -(df+1)/2);
            var f = (i === 0 || i == iterations) ? 1 : (i % 2) ? 4 : 2;
            sum += e * f;
        }

        sum *= (1/Math.sqrt(Math.PI * df) * this.gamma((df + 1)/2)/this.gamma(df/2)) * stepSize/3;

        // console.log('SIMMON SUM * CONSTANT', sum);
        // console.log('P-VALUE', t > 0 ? 1 - 2 * sum : 0.5 + sum);

        //Multiply by 2 for 2-tail probabilitiy
        return t > 0 ? 1 - 2 * sum : 0.5 + sum;
    },
    chiSqrToPvalue: function(x2, df){
        if(x2 < 0 || df < 1) return 0;

        return 1 - this.incompleteGamma(df/2, x2/2)/this.gamma(df/2);
    },
    chiSqr: function(data){
        // console.log(_.pluck(data, 'actualValue'));
        // console.log(_.pluck(data, 'projectedValue'));
        return _.sum(_.map(data, function(x){
            return Math.pow(x.actualValue - x.projectedValue, 2)/x.projectedValue;
        }));
    },
    r_tStat: function(r, df){
        // console.log('CALCULATED TSTAT IS', r * Math.sqrt(df)/Math.sqrt(1-Math.pow(r, 2)));
        return r * Math.sqrt(df)/Math.sqrt(1-Math.pow(r, 2));
    },
    pearsonR: function(data){
        // console.log(_.pluck(data, 'actualValue'));
        // console.log(_.pluck(data, 'projectedValue'));

        var xbar = this.mean(_.pluck(data, 'actualValue'));
        var ybar = this.mean(_.pluck(data, 'projectedValue'));

        var r = _.sum(_.map(data, function(d){return (d.actualValue - xbar) * (d.projectedValue - ybar);})) /
                  Math.sqrt(_.sum(_.map(data, function(d){return Math.pow(d.actualValue - xbar, 2);})) * _.sum(_.map(data, function(d){return Math.pow(d.projectedValue - ybar, 2);})));

        return r;
    },
    mean: function(data){
        return _.sum(data)/data.length;
    },
    median: function(data){

        //Javascript defaults to lexi sort order - make it sort by numerical order
        var sortedData = data.slice(0).sort(function(a, b){
            return a - b;
        });

        //If odd just take the middle number, otherwise take mean of middle 2 numbers
        return sortedData.length % 2 ? sortedData[Math.floor(sortedData.length/2)] : (sortedData[sortedData.length/2] + sortedData[sortedData.length/2 - 1])/2;
    },
    stdDeviation: function(data){
        var dataMean = this.mean(data);
        return Math.sqrt(1/data.length * _.sum(_.map(data, function(x){
            return Math.pow(x - dataMean, 2);
        })));
    },
    medianAbsoluteDeviation: function(data){
        var dataMedian = this.median(data);
        return this.median(_.map(data, function(x){
            return Math.abs(x - dataMedian);
        }));
    }
};

module.exports = statUtils;
