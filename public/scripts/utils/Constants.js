var constants = {
    'LOADED': 'STORE_LOADED',
    'LOADING': 'STORE_LOADING',
    'ERROR': 'STORE_ERROR'
};

module.exports = constants;
